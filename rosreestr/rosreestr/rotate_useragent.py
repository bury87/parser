#!/usr/bin/python
#-*-coding:utf-8-*-

import random
import logging
from scrapy.contrib.downloadermiddleware.useragent import UserAgentMiddleware

class RotateUserAgentMiddleware(UserAgentMiddleware):
    def __init__(self, user_agent=''):
        self.user_agent = user_agent
        self.user_agent_list = self.user_agent_dict()

    def process_request(self, request, spider):
        ua = random.choice(self.user_agent_list)
        if ua:
            request.headers.setdefault('User-Agent', ua)

    def user_agent_dict(self):
        logger = logging.getLogger()
        try:
            fp = open('useragent.txt','r')
            user_dict = fp.read().split('\n')
            fp.close()
            return user_dict
        except IOError:
            logger.error('Cannot open french dict!')