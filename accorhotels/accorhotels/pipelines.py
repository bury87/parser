# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from json import  dumps
import json
from codecs import getwriter
from sys import stdout
import logging



class AccorhotelsPipeline(object):
    def __init__(self):
        self.sout = getwriter("utf8")(stdout)

        self.reestr = set()


    def process_item(self, item, spider):
        logger = logging.getLogger()
        if not item['link'] in self.reestr:
            self.reestr.add(item['link'])
            self.sout.write(dumps(dict(item), ensure_ascii=False) + "\n")
        return item


