import re

def phone_number_format(phone):
    phonePattern = re.compile(r'(\d{3,4})\D*(\d{2,3})\D*(\d{2,3})\D*(\d*)\D*$')
    phone = phonePattern.search(phone).groups()
    print phone
    result = '+38({}){}-{}-{}'
    result = result.format(phone[0], phone[1], phone[2], phone[2])
    return result


test = '(0412) 48-10-12;'
print phone_number_format(test)