# coding: utf8
import scrapy
import json
from scrapy.selector import Selector as SL
from providna.items import ProvidnaItem

class ProvidnaSpider(scrapy.Spider):
    name = "providna"
    def start_requests(self):
        url = 'http://www.providna.ua/ru/content/filialy-i-tsentry-prodazh'
        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        urls = response.xpath(".//ul[@class='menu']/li[position()>1]/a/@href").extract()
        for url in urls:
            yield scrapy.Request(response.urljoin(url), callback=self.parse1_ru)

    def parse1_ru(self, response):
        ru = []
        tr = response.xpath(".//table[@class='tabaddress']/tbody/tr[position()>1]").extract()
        if len(tr)==0:
            tr = response.xpath(".//table[@class='tabaddress']/tr[position()>1]").extract()


        for htmltr in tr:
            filiya ={}
            filiya['city'] = SL(text=htmltr).xpath('.//td[position()=1]/text()').extract_first()
            filiya['time'] = SL(text=htmltr).xpath('.//td[position()=2]/text()').extract_first()
            filiya['address'] = SL(text=htmltr).xpath('.//td[position()=3]/text()').extract_first()
            filiya['admn_area'] = response.xpath(".//li[@class='leaf']/span/a/text()").extract_first()
            filiya['phone'] = SL(text=htmltr).xpath('.//td[position()=4]/text()').extract_first()
            ru.append(filiya)
        url_ua = response.xpath(".//div[@id='block-translation-0']/div/div/ul/li[position()=2]/a/@href").extract_first()
        yield scrapy.Request(response.urljoin(url_ua), callback=self.parse1_ua, meta={'ru': ru})

    def parse1_ua(self, response):
        ru = response.meta['ru']
        ua = []
        tr = response.xpath(".//table[@class='tabaddress']/tbody/tr[position()>1]").extract()
        if len(tr)==0:
            tr = response.xpath(".//table[@class='tabaddress']/tr[position()>1]").extract()
        for htmltr in tr:
            filiya = {}
            filiya['city'] = SL(text=htmltr).xpath('.//td[position()=1]/text()').extract_first()
            filiya['time'] = SL(text=htmltr).xpath('.//td[position()=2]/text()').extract_first()
            filiya['address'] = SL(text=htmltr).xpath('.//td[position()=3]/text()').extract_first()
            filiya['admn_area'] = response.xpath(".//ul[@class='menu']/li/span[@class='active_link']/a/text()").extract_first()
            filiya['phone'] = SL(text=htmltr).xpath('.//td[position()=4]/text()').extract()
            ua.append(filiya)
        item = ProvidnaItem()
        item['ru'] = ru
        item['ua'] = ua
        yield item