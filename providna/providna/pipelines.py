# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from lxml import etree
from codecs import getwriter
from sys import stdout
import re
import time

class ProvidnaPipeline(object):

    def __init__(self):
        self.sout = getwriter("utf8")(stdout)
        self.root = etree.Element('companies')

    def process_item(self, item, spider):

        for ru,ua in zip(item['ru'],item['ua']):
            company = etree.Element('company')
            name_ru = etree.Element('name')
            name_ru.set('lang', 'ru')
            name_ru.text = u'Провидна»'
            name_ua = etree.Element('name')
            name_ua.set('lang', 'ua')
            name_ua.text = u'Провідна»'

            locality_ru = etree.Element('locality-name')
            locality_ru.set('lang', 'ru')
            locality_ru.text = ru['city'].strip()
            locality_ua = etree.Element('locality-name')
            locality_ua.set('lang', 'ua')
            locality_ua.text = ua['city'].strip()

            country_ru = etree.Element('country')
            country_ru.set('lang', 'ru')
            country_ru.text = u'Украина'
            country_ua = etree.Element('country')
            country_ua.set('lang', 'ua')
            country_ua.text = u'Україна'

            adnm_area_ru = etree.Element('admn-area')
            adnm_area_ru.set('lang', 'ru')
            adnm_area_ru.text = ru['admn_area'].strip()
            adnm_area_ua = etree.Element('admn-area')
            adnm_area_ua.set('lang', 'ua')
            adnm_area_ua.text = ua['admn_area'].strip()

            address_ru = etree.Element('address')
            address_ru.set('lang', 'ru')
            address_ru.text = self.address_format(ru['address'].strip())
            address_ua = etree.Element('address')
            address_ua.set('lang', 'ua')
            address_ua.text = self.address_format(ua['address'].strip())

            unix_time = int(time.time())

            actualization_date = etree.Element('actualization-date')
            actualization_date.text = str(unix_time)

            for phnmbr in ua['phone']:
                if re.search(r'\d',phnmbr).groups():
                    phone = etree.Element('phone')
                    number = etree.Element('number')
                    number.text = self.phone_number_format(phnmbr.strip())
                    phone.append(number)
                    company.append(phone)

            rubric_id1 = etree.Element('rubric-id')
            rubric_id1.text = u'184105536'

            rubric_id2 = etree.Element('rubric-id')
            rubric_id2.text = u'184105534'

            url = etree.Element('url')
            url.text = u'http://www.providna.ua/'
            company.append(name_ru)
            company.append(name_ua)
            company.append(locality_ru)
            company.append(locality_ua)
            company.append(country_ru)
            company.append(country_ua)
            company.append(adnm_area_ru)
            company.append(adnm_area_ua)
            company.append(address_ru)
            company.append(address_ua)
            company.append(rubric_id2)
            company.append(rubric_id1)
            company.append(url)
            company.append(actualization_date)

            self.root.append(company)

        return item

    def close_spider(self, spider):
        self.xml = etree.tostring(self.root, encoding='UTF-8', xml_declaration=True, pretty_print=True)
        self.sout.write(self.xml.decode('utf-8'))

    def phone_number_format(self,phone):
        phonePattern = re.compile(r'(\d{3,4})\D*(\d{2,3})\D*(\d{2,3})\D*(\d*)\D*$')
        phone = phonePattern.search(phone).groups()
        result = '+38({}){}-{}-{}'
        result = result.format(phone[0], phone[1], phone[2], phone[2])
        return result

    def address_format(self, address):
        result = re.sub(r'["\\n]','',address)
        result = re.sub(r'\s+', ' ', result)
        return result




